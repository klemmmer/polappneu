//
//  DetailViewController.swift
//  politikerApp
//
//  Created by Clemens Harnischmacher on 16.11.16.
//  Copyright © 2016 Clemens Harnischmacher. All rights reserved.
//

import UIKit



class DetailViewController: UIViewController {
    
    enum MyErrors : Error {
        case missing
    }
    
    @IBOutlet weak var politikerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    
    
    var politiker: PolitikerDetails! {
        didSet (newPolitiker) {
            self.refreshUI()
            
        }
    }
    
    func refreshUI() {
        
        //Wandle den String des Bildes in NSData um
        var data: NSData?
        
        
        if politiker != nil {
            if politiker.bild != nil {
                do {
                    data = try getDataImage(politiker.bild)
                } catch _ {
                    
                }
            }
            //let data: NSData = NSData(base64Encoded: politiker.bild  , options: .ignoreUnknownCharacters)!
            //Setze das richtige Bild, mit Abfangen falls keins vorhanden
            var decodedimage: UIImage?
            do {
                decodedimage = try getPicture(data as! Data)
            } catch _ {
                //Hier wird ein Standard Logo gesetzt, falls kein Logo vorhanden
                decodedimage = UIImage(named: "steinmeier.jpg")
            }
            politikerImageView?.image = decodedimage
        }
        
        nameLabel?.text = politiker.name
        positionLabel?.text = politiker.position
        
    }
    
    
    
    func firstUI() {
        nameLabel?.text = "Name"
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        /*if let detail = self.detailItem {
         if let label = self.detailDescriptionLabel {
         label.text = detail.description
         }
         }*/
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        firstUI()
        self.configureView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var detailItem: NSDate? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    func getDataImage(_ imageString: String) throws -> NSData{
        guard let data  = NSData(base64Encoded: imageString  , options: .ignoreUnknownCharacters) else {
            throw MyErrors.missing
        }
        return data
    }
    
    
    //Wandelt NSData in UIImage um
    func getPicture(_ partyLogo: Data) throws -> UIImage{
        guard let decodedimage = UIImage(data: partyLogo) else {
            throw MyErrors.missing
        }
        return decodedimage
    }
}

extension DetailViewController: PolitikerSelectionDelegate {
    func politikerSelected(newPolitiker: PolitikerDetails) {
        politiker = newPolitiker
    }
}

