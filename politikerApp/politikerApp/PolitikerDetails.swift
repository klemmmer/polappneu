//
//  PolitikerDetails.swift
//  politikerApp
//
//  Created by Clemens Harnischmacher on 16.11.16.
//  Copyright © 2016 Clemens Harnischmacher. All rights reserved.
//

import Foundation

class PolitikerDetails{
    var id: String!
    var name: String!
    var position: String!
    var partei: String!
    var bild: String!
    var kreis: String!
    var gemeinde: String!
    
    init(id: String, name: String, position: String, partei: String, bild: String, kreis: String, gemeinde: String){
        self.id = id
        self.name = name
        self.position = position
        self.partei = partei
        self.bild = bild
        self.kreis = kreis
        self.gemeinde = gemeinde
    }
    
}
