//
//  AppDelegate.swift
//  politikerApp
//
//  Created by Clemens Harnischmacher on 16.11.16.
//  Copyright © 2016 Clemens Harnischmacher. All rights reserved.
//

import UIKit
import Firebase

var politikerArray = [PolitikerDetails]()
//Der Array sollte mal noch erweitert werden
var parteienArray = ["AfD", "BIW", "BVB / FREIE WÄHLER", "CDU", "CSU",  "DIE LINKE", "Die PARTEI", "FAMILIE", "FDP", "FREIE WÄHLER", "GRÜNE", "LKR", "NPD", "ÖDP", "PIRATEN", "SPD", "SSW"]
var positionenArray = ["MdB", "MdL", "Mitglied", "Stadtrat", "Minister"]
var laenderArray = ["Baden-Württemberg", "Bayern", "Berlin", "Brandenburg", "Bremen", "Hamburg", "Hessen", "Mecklenburg-Vorpommern", "Niedersachsen", "Nordrhein-Westfalen", "Rheinland-Pfalz", "Saarland", "Sachsen", "Sachsen-Anhalt", "Schleswig-Holstein", "Thüringen"]
var firstUse = true

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FIRApp.configure()
        FIRDatabase.database().persistenceEnabled = true
        
        let tabBarController = self.window!.rootViewController as! UITabBarController
        let splitViewController = tabBarController.viewControllers?.first as! UISplitViewController
        let leftNavController = splitViewController.viewControllers.first as! UINavigationController
        let detailViewController = splitViewController.viewControllers.last as! DetailViewController
        let firstViewController = leftNavController.topViewController as! FirstViewController
        
        let firstPolitiker = politikerArray.first
        detailViewController.politiker = firstPolitiker
        firstViewController.delegate = detailViewController
        
        return true
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

