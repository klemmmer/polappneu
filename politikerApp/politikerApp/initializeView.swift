//
//  initializeView.swift
//  politikerApp
//
//  Created by Clemens Harnischmacher on 20.11.16.
//  Copyright © 2016 Clemens Harnischmacher. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseInstanceID

class initializeView: UIViewController ,UIPickerViewDataSource,UIPickerViewDelegate , UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var positionTextFieldOutlet: UITextField!
    @IBOutlet weak var nameTextFieldOutlet: UITextField!
    @IBOutlet weak var setPolitikerImageViewOutlet: UIImageView!
    @IBOutlet weak var parteiPickerViewOutlet: UIPickerView!
    @IBOutlet weak var fertigButton: UIButton!
    @IBOutlet weak var kreisTextFieldOutlet: UITextField!
    @IBOutlet weak var gemeindeTextFieldOutlet: UITextField!
    @IBOutlet weak var autocompleteTableView: UITableView!
    @IBOutlet weak var landPickerViewOutlet: UIPickerView!
    @IBOutlet weak var choosePictureButton: UIButton!
    
    var autocompleteUrls = [String]()
    let imagePicker = UIImagePickerController()
    var polBild = UIImage(named:"roth.jpg")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
        landPickerViewOutlet.dataSource = self
        landPickerViewOutlet.delegate = self
        landPickerViewOutlet.tag = 1
        parteiPickerViewOutlet.dataSource = self
        parteiPickerViewOutlet.delegate = self
        parteiPickerViewOutlet.tag = 2
        //Verstecke den Backbutton
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        // Do any additional setup after loading the view, typically from a nib.
        
        //Notwendiges für Autocomplete
        autocompleteTableView.delegate = self
        autocompleteTableView.dataSource = self
        autocompleteTableView.isScrollEnabled = true
        autocompleteTableView.isHidden = true
        
        positionTextFieldOutlet.delegate = self
        positionTextFieldOutlet.tag = 1
    }
    
    @IBAction func photosAusLibraryButton(_ sender: Any) {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        choosePictureButton.tintColor = UIColor.clear
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        /*if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            setPolitikerImageViewOutlet.contentMode = .scaleAspectFit
            setPolitikerImageViewOutlet.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)*/
        
        var newImage: UIImage
        
        if let possibleImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            newImage = possibleImage
            polBild = possibleImage
        } else if let possibleImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            newImage = possibleImage
            polBild = possibleImage
        } else {
            return
        }
        
        // do something interesting here!
        print(newImage.size)
        setPolitikerImageViewOutlet.contentMode = .scaleAspectFit
        setPolitikerImageViewOutlet.image = newImage
        
        dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func fertigButtonAction(_ sender: Any) {
        let politiker = politikerArray[(politikerArray.count-1)]
        let idCalculate = Int(politiker.id)! + 1
        let id = String(idCalculate)
        if(!(nameTextFieldOutlet.text?.isEmpty)!) {
            if(!(positionTextFieldOutlet.text?.isEmpty)!) {
                let name = nameTextFieldOutlet.text ?? ""
                let position = positionTextFieldOutlet.text ?? ""
                let partei = parteienArray[parteiPickerViewOutlet.selectedRow(inComponent: 0)]
                //Bild in NSDAta umwandenl und hochladen
                
                let smallImage = resizeImage(image: polBild!)
                let imageData: NSData = UIImagePNGRepresentation(smallImage!)! as NSData
                let strBase64:String = imageData.base64EncodedString(options: .lineLength64Characters)
                
                let kreis = kreisTextFieldOutlet.text ?? ""
                let gemeinde = gemeindeTextFieldOutlet.text ?? ""
                let politikerNew = ["Id": id, "Name": name, "Position":position, "Partei": partei, "Bild": strBase64, "Kreis": kreis, "Gemeinde": gemeinde] as [String : Any]
                
                let politikerRef = FIRDatabase.database().reference(withPath: "Politiker")
                let politikerChildRef = politikerRef.child(id)
                
                
                politikerChildRef.setValue(politikerNew as NSDictionary)
            } else {
                print("bitte Position angeben")
            }
        }
        else {
            print("bitte Namen angeben")
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        autocompleteTableView.isHidden = false
        let substring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if(substring.isEmpty) {
            autocompleteTableView.isHidden = true
        } else {
            searchAutocompleteEntriesWithSubstring(substring: substring, tag: textField.tag)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        autocompleteTableView.isHidden = true
        return true
    }
    
    //Vergleicht was der Nutzereingegeben hat mit dem, was vorgegeben war.
    //So kann eine Autovervollständigung angeboten werden
    func searchAutocompleteEntriesWithSubstring(substring: String, tag: Int)
    {
        autocompleteUrls.removeAll(keepingCapacity: false)
        switch tag {
            
        case 1:
            for curString in positionenArray
            {
                var myString:NSString! = curString as NSString
                myString = myString.lowercased as NSString!
                let substringLowercase = substring.lowercased()
                let substringRange :NSRange! = myString.range(of: substringLowercase)
                
                if (substringRange.location  != 9223372036854775807)
                {
                    autocompleteUrls.append(curString)
                }
            }
            
            autocompleteTableView.reloadData()
        default:
            print("Didn't detect any field")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return autocompleteUrls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = self.autocompleteTableView.dequeueReusableCell(withIdentifier: "testCell", for: indexPath)
        let index = indexPath.row as Int
        cell.textLabel!.text = autocompleteUrls[index]
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //fix auf 160 eingsetellt
        return 30//tableView.bounds.height*0.25
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell : UITableViewCell = autocompleteTableView.cellForRow(at: indexPath)!
        positionTextFieldOutlet.text = selectedCell.textLabel!.text
        
        autocompleteTableView.isHidden = true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count = 0
        switch pickerView.tag {
            
        case 1:
            count = laenderArray.count
            
        case 2:
            count = parteienArray.count
            
        default:
            print("Habe nicht den Picker gefunden")
        }
        return count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var tempString = ""
        switch pickerView.tag {
            
        case 1:
            tempString = laenderArray[row]
        case 2:
            tempString =  parteienArray[row]
            
        default:
            tempString.append("Fehler")
        }
        return tempString
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //Was mit der Zeile machen, die ausgewählt wurde?
        //Dieser Wer muss dann in Firebase gespeichert werden
        //nameTextFieldOutlet.text = parteienArray[row]
    }
    
    func bildHochladen(){
        //Bild in NSDAta umwandenl und hochladen
        let image = UIImage(named: "roth.jpg")
        let smallImage = resizeImage(image: image!)
        let imageData: NSData = UIImagePNGRepresentation(smallImage!)! as NSData
        
        let strBase64:String = imageData.base64EncodedString(options: .lineLength64Characters)
        
        //let dataString = NSString(data: imageData, encoding: String.Encoding.utf8.rawValue)//NSString(data: imageData, encoding: .UInt)
        /*
         let partyIDDestination = politikerRef.child("0005")
         let ratingUpload = ["Bild":strBase64] as [String : Any]
         
         partyIDDestination.updateChildValues(ratingUpload ,  withCompletionBlock: {
         (error:Error?, ref:FIRDatabaseReference!) in
         if (error != nil) {
         print("Error uploading: \(error)")
         } else {
         }
         })*/
        
    }
    
    func resizeImage(image: UIImage) -> UIImage? {
        let scale = 100 / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: 100, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: 100, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}
