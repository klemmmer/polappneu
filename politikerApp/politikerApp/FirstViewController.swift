//
//  FirstViewController.swift
//  politikerApp
//
//  Created by Clemens Harnischmacher on 16.11.16.
//  Copyright © 2016 Clemens Harnischmacher. All rights reserved.
//

import UIKit
import Firebase
import FirebaseInstanceID

protocol PolitikerSelectionDelegate: class {
    func politikerSelected(newPolitiker: PolitikerDetails)
}


class FirstViewController: UITableViewController {
    
    var valueToPass:String!
    
    
    enum MyErrors : Error {
        case missing
    }
    
    weak var delegate: PolitikerSelectionDelegate?
    
    @IBOutlet var politikerTableView: UITableView!
    var detailViewController: DetailViewController? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        let politikerRef = FIRDatabase.database().reference(withPath: "Politiker")
        
        if(!firstUse) {
            let firstViewController = (self.storyboard?.instantiateViewController(withIdentifier: "firstView"))! as UIViewController
            self.navigationController?.pushViewController(firstViewController, animated: true)
            firstUse = false
        }
        
        //Aktuelle Liste der Politiker runterladen
        politikerRef.observe(.value, with: { snapshot in
            do {
                try self.loadPolitikerToArray(snapshot)
            } catch _ {
            }
        })
        
    }
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController!, ontoPrimaryViewController primaryViewController: UIViewController!) -> Bool {
        // Return YES to prevent UIKit from applying its default behavior
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table View
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return politikerArray.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CustomCellTableViewCell = self.politikerTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomCellTableViewCell
        
        let politiker = politikerArray[indexPath[1]]
        
        //Setze die Daten aus dem Array in die View
        //Zuerst name, dann Position, Bild
        cell.nameLabel.text = politiker.name
        cell.positionLabel.text = politiker.position
        
        //Wandle den String des Bildes in NSData um
        let data: NSData = NSData(base64Encoded: politiker.bild  , options: .ignoreUnknownCharacters)!
        //Setze das richtige Bild, mit Abfangen falls keins vorhanden
        var decodedimage: UIImage?
        do {
            decodedimage = try getPicture(data as Data)
        } catch _ {
            //Hier wird ein Standard Logo gesetzt, falls kein Logo vorhanden
            decodedimage = UIImage(named: "steinmeier.jpg")
        }
        cell.politikerImageView.image = decodedimage
        
        //Setze die Farbe entsprechend der Partei
        let partei: String = politiker.partei
        switch partei {
        case "CDU/CSU":
            cell.parteiImageView.backgroundColor = UIColor.black
        case "SPD":
            cell.parteiImageView.backgroundColor = UIColor.red
        case "Bündnis 90/Die Grünen":
            cell.parteiImageView.backgroundColor = UIColor.green
        case "Die Linke":
            cell.parteiImageView.backgroundColor = UIColor.purple
        case "FDP":
            cell.parteiImageView.backgroundColor = UIColor.yellow
        case "AFD":
            cell.parteiImageView.backgroundColor = UIColor.blue
        default:
            print("Keine Partei hinterlegt")
        }
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //fix auf 160 eingsetellt
        return 160//tableView.bounds.height*0.25
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedPolitiker = politikerArray[indexPath.row]
        self.delegate?.politikerSelected(newPolitiker: selectedPolitiker)
        if let detailViewController = self.delegate as? DetailViewController {
            splitViewController?.showDetailViewController(detailViewController, sender: nil)
        }
    }
    
    
    func loadPolitikerToArray(_ snapshot: FIRDataSnapshot) throws {
        
        let enumerator = snapshot.children
        while let rest = enumerator.nextObject() as? FIRDataSnapshot {
            
            let id = (rest.value! as AnyObject).object(forKey: "Id") as! String
            let name = (rest.value! as AnyObject).object(forKey: "Name") as! String
            let partei = (rest.value! as AnyObject).object(forKey: "Partei") as! String
            let position = (rest.value! as AnyObject).object(forKey: "Position") as! String
            var bild: String?
            if ((rest.value! as AnyObject).object(forKey: "Bild") != nil) {
                bild = (rest.value! as AnyObject).object(forKey: "Bild") as? String
            }
            else {
                let image = UIImage(named: "roth.jpg")
                let smallImage = resizeImage(image: image!)
                let imageData: NSData = UIImagePNGRepresentation(smallImage!)! as NSData
                
                bild = imageData.base64EncodedString(options: .lineLength64Characters)
            }
            // let data = bild.data(using: .utf8)
            
            let kreis = (rest.value! as AnyObject).object(forKey: "Kreis") ?? ""
            let gemeinde = (rest.value! as AnyObject).object(forKey: "Gemeinde") ?? ""
            
            
            politikerArray.append(PolitikerDetails(id:id, name:name, position:position, partei:partei, bild:bild!, kreis:kreis as! String, gemeinde:gemeinde as! String))
        }
        tableView.reloadData()
        
        /* let appDelegate = UIApplication.shared.delegate as! AppDelegate
         let tabBarController = appDelegate.window!.rootViewController as! UITabBarController
         let splitViewController = tabBarController.viewControllers?.first as! UISplitViewController
         let detailViewController = splitViewController.viewControllers.last as! DetailViewController
         
         let firstPolitiker = politikerArray.first
         detailViewController.politiker = firstPolitiker*/
        
    }
    
    
    //Wandelt NSData in UIImage um
    func getPicture(_ partyLogo: Data) throws -> UIImage{
        guard let decodedimage = UIImage(data: partyLogo) else {
            throw MyErrors.missing
        }
        return decodedimage
    }
    
    
    func resizeImage(image: UIImage) -> UIImage? {
        let scale = 100 / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: 100, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: 100, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    
    //Wandelt NSData in UIImage um
    func getLogo(_ partyLogo: Data) throws -> UIImage{
        guard let decodedimage = UIImage(data: partyLogo) else {
            throw MyErrors.missing
        }
        return decodedimage
    }
    
    
    
    //Hiermit können Objekte aus der Tableview entfernt werden
    //TODO: Der Politiker muss dann aus den Followern entfernt werden
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            politikerArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    
    /*
     //Hier wird an die Detailview übergeben
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     if segue.identifier == "showDetail" {
     if let indexPath = self.tableView.indexPathForSelectedRow {
     let object = objects[indexPath.row] as! NSDate
     let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
     controller.detailItem = object
     controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
     controller.navigationItem.leftItemsSupplementBackButton = true
     }
     }
     }*/
    
    
    /*'
     func bildHochladen(){
     //Bild in NSDAta umwandenl und hochladen
     let image = UIImage(named: "roth.jpg")
     let smallImage = resizeImage(image: image!)
     let imageData: NSData = UIImagePNGRepresentation(smallImage!)! as NSData
     
     let strBase64:String = imageData.base64EncodedString(options: .lineLength64Characters)
     
     //let dataString = NSString(data: imageData, encoding: String.Encoding.utf8.rawValue)//NSString(data: imageData, encoding: .UInt)
     /*
     let partyIDDestination = politikerRef.child("0005")
     let ratingUpload = ["Bild":strBase64] as [String : Any]
     
     partyIDDestination.updateChildValues(ratingUpload ,  withCompletionBlock: {
     (error:Error?, ref:FIRDatabaseReference!) in
     if (error != nil) {
     print("Error uploading: \(error)")
     } else {
     }
     })*/
     
     }*/
}

