//
//  PrimarySplitViewController.swift
//  politikerApp
//
//  Created by Clemens Harnischmacher on 16.11.16.
//  Copyright © 2016 Clemens Harnischmacher. All rights reserved.
//

import UIKit


class PrimarySplitViewController: UISplitViewController, UISplitViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
    }
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
       return true
    }
    
}
