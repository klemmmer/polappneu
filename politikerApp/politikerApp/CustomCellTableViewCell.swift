//
//  CustomCellTableViewCell.swift
//  politikerApp
//
//  Created by Clemens Harnischmacher on 16.11.16.
//  Copyright © 2016 Clemens Harnischmacher. All rights reserved.
//

import Foundation
import UIKit

class CustomCellTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var politikerImageView: UIImageView!
    @IBOutlet weak var parteiImageView: UIImageView!
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        
    }
    
    
    
}
